
//arithmetic operators

let x = 1500;
let y = 7500;
let sum = x + y;
console.log("Addition: " + sum);
//result 9000

let difference = y - x;
console.log("Subtraction " + difference);

let product = x * y;
console.log("Multiplication " + product);

let qoutient = y / x;
console.log("division " + qoutient);

let remainder = y % x;
console.log("Modulo " + remainder);

//Assignment operator
	//basic assignment operator (=)

let assignmentNumber = 8;
	//Addition Assignment operator (+=)
//assignemntNumber = assignmnetNumber + 2;
//assignmentNumber = 8 + 2;

assignmentNumber += 2;
console.log("Addition Assignment " + assignmentNumber);
//result = 10

assignmentNumber += 2;
console.log("Adition assignment " + assignmentNumber);
//result = 12

assignmentNumber -= 2;
console.log("Subtraction assignment " + assignmentNumber);
//result = 10

assignmentNumber *= 2;
console.log("Multiplication assignment " + assignmentNumber);
//result = 20

assignmentNumber /+ 2;
console.log("division assignment " + assignmentNumber);
//result = 20

let string1 = "Boston";
let string2 = "Celtics";
string1 += string2;
console.log(string1);
//result = BostonCeltics
// string1 = string1 + string2

//Multiple operation and parenthesis
let mdas = 1 + 2 -3 * 4 / 5;
console.log(mdas);
//result = 0.6

let pemdas = 1 + (2 -3) * (4 / 5);
console.log(pemdas);
// result 0.2

//Increment and decrement
let z = 1;

let increment = ++ z;
console.log("pre increment; " + increment);
//result = 2
console.log("value of z: " + z);
//result = 2

increment = z ++;
console.log("post increment " + z);
//result = 2

console.log("value of z " + z);
//result = 3

let decrement= --z;
console.log("pre decrement " + z);
//result 2
console.log("value of z " + z);
//result 2

decrement= --z;
console.log("post decrement" + z);
//result 2
console.log("value of z" + z);
//result 1

//type coercion

let numA = '10';
let numB = 12;
let coersion = numA + numB;
console.log(coersion); 
//result 1012
console.log(typeof coersion);
//result string

let numC = 16;
let numD = 14;
let nonCoersion = numC + numD;
console.log(nonCoersion);
//result 30
console.log(typeof nonCoersion);
//result number

let numE = true + 1;
console.log(numE);
//result 2

let numF = false + 1;
console.log(numF);
//result 1

//equality operator (==)
console.log(1 ==1);
//result true

console.log(1 ==2);
//result false

console.log(1 == '1');
//result true

console.log(false == 0);
//result true

console.log('johnny' == 'johnny');
// result true

console.log('Johnny' == 'johnny');
// result false

// inquality operator (!=)

console.log(1 != 1);
// result false

console.log(1 != 2);
// result true

console.log(1 != '1');
// result false

console.log(0 != false);
// result false

console.log('johnny' != 'johnny');
// result false

console.log('Johnny' != 'johnny')
// result true

//strict equality operator (===)

console.log(1 === 1);
// result true

console.log(1 === '1');
// result false

console.log('johnny' === 'johnny');
// result true

//strict inequality (!==)
console.log(1 !== 1);
// result false

console.log(1 !== 2);
// result true

console.log(1 !== '1');
// reult true

console.log('johnny' !== 'johnny');
// result false

//relational operator (<> <= >=)
let a = 50;
let b = 65;

//greater than >
let isGreaterthan = a > b;
console.log(isGreaterthan);
// reult false

//less than
let isLessthan = a < b;
console.log(isLessthan);
// result true

let isGTE = a >= b;
console.log(isGTE);
// result false

let isLTE = a <= b;
console.log(isLTE);
// result true

//logical operator 
 
 //AND (&&)

let isAdmin = false;
let isRegisted = true;
let isLegalAge = true;

let authorization = isAdmin && isRegisted;
console.log(authorization);
//result // false

let authorization2 = isLegalAge && isRegisted;
console.log(authorization2);
//reult true

let authorization3 = isAdmin && isLegalAge;
//result false

let random = isAdmin && false;
// result false

let requiredLevel = 95;
let requiredAge = 18;

let authorization4 = isRegisted && requiredLevel === 25;
console.log(authorization4);
//result false

let authorization5 = isRegisted && isLegalAge && requiredLevel === 95;
//result true

let userName = 'gamer2022';
let userName2 = 'theTinker';
let userAge = 15;
let userAge2 = 26;

let registration1 = userName.length > 8 && userAge >= requiredAge;
console.log(registration1);
//false

let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
console.log(registration2);
//OR (||)

let userLevel = 100;
let userLevel2 = 65;

let guildRequirement = isRegisted && userLevel >= requiredLevel && userAge >=requiredAge;
console.log(guildRequirement);
//result false

let guildRequirement2 = isRegisted || userLevel2 >= requiredLevel || userAge2 >= requiredAge;
console.log(guildRequirement2); 
//true

let guildRequirement3 = userLevel >= requiredLevel || userAge >= requiredAge;
console.log(guildRequirement3); 
//true

let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin); 
//false

//NOT operator (!)

console.log(!isRegisted);
//false

let opposite1 = !isAdmin;
console.log(opposite1);
//true








































